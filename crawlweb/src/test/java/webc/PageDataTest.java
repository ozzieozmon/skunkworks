package webc;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.List;

import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

/**
 * Created by ozzie on 10/3/15.
 */
public class PageDataTest {

    @Before
    public void setUp() throws Exception {

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testPageData() throws Exception {
        String testValue = "http://www.ucf.edu/alumni-giving/";
        PageData testPage = spy(new PageData());
        InputStream is = getClass().getClassLoader().getResourceAsStream("test.html");
        String myString = IOUtils.toString(is, "UTF-8");
        Document doc = Jsoup.parse(myString, "http://test.com/");
        doReturn(doc).when(testPage).getDocument("http://wwww.test.com");
        testPage.getPageData("http://wwww.test.com");
        Assert.assertTrue(testPage.isFound());
        List<String> resultList=testPage.getLinks();
        Assert.assertEquals(testPage.getLinks().size(), 73);
        Assert.assertTrue(testPage.getLinks().contains(testValue));
        //  File input = new File("test.html");
    }
}
