package webc;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.*;

/**
 * Created by ozzie on 10/3/15.
 */
public class WebPageProcessorTest {
    ExecutorService executor = mock(ExecutorService.class);
    PageProcessor findWordPageProcessor = mock(PageProcessor.class);
    String testLink = "http://www.ucf.edu";
    PageData testPage;

    @Before
    public void setUp() throws Exception {
        doNothing().when(executor).execute(isA(Runnable.class));
        testPage = getPage();
        /*Registering page processors */
        WebPageProcessor.registerPageProcessor(findWordPageProcessor);
        WebPageProcessor.setExecutor(executor);
        WebPageProcessor.setQueue(new LinkedBlockingQueue<PageData>());
    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testProcess() throws Exception {
        WebPageProcessor.getQueue().add(testPage);
        WebPageProcessor testWebPageProcessor = new WebPageProcessor();
        testWebPageProcessor.processPage();
        Assert.assertFalse(WebPageProcessor.getQueue().contains(testPage));
        verify(findWordPageProcessor, times(1)).processPage(testPage);
    }

    @Test
    public void testEnqueue() throws Exception {
        WebPageProcessor testWebPageProcessor = new WebPageProcessor();
        testWebPageProcessor.enqueue(testPage);
        Assert.assertTrue(WebPageProcessor.getQueue().contains(testPage));
        verify(executor, times(1)).execute(isA(Runnable.class));
    }

    private PageData getPage() throws Exception {
        PageData testPage = spy(new PageData());
        InputStream is = getClass().getClassLoader().getResourceAsStream("test.html");
        String myString = IOUtils.toString(is, "UTF-8");
        Document doc = Jsoup.parse(myString, testLink);
        doReturn(doc).when(testPage).getDocument(testLink);
        testPage.getPageData(testLink);
        return testPage;
    }
}
