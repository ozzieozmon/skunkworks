package webc;

import org.apache.commons.io.IOUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.InputStream;
import java.util.concurrent.ExecutorService;

import static org.mockito.Mockito.*;

/**
 * Created by ozzie on 10/3/15.
 */
public class WebCrawlerTest {
    WebPageProcessor webPageProcessor = mock(WebPageProcessor.class);
    ExecutorService executor = mock(ExecutorService.class);
    String testLink = "http://www.ucf.edu";

    @Before
    public void setUp() throws Exception {

        doNothing().when(executor).execute(isA(Runnable.class));

        /*Registering wen page processors */
        WebPageProcessor.registerPageProcessor(new FindWordPageProcessor("mystery"));
        WebPageProcessor.registerPageProcessor(new FindWordPageProcessor("java"));
        WebPageProcessor.setExecutor(executor);
        /*Initializing web crawler*/
        WebCrawler.init(testLink);
        WebCrawler.setExecutor(executor);

    }

    @After
    public void tearDown() throws Exception {

    }

    @Test
    public void testCrawl() throws Exception {
        PageData testPage = getPage();
        WebCrawler testWebCrawler = spy(new WebCrawler());
        doReturn(testPage).when(testWebCrawler).getPageData(testLink);
        testWebCrawler.setWebPageProcessor(webPageProcessor);
        testWebCrawler.crawl();
        Assert.assertEquals(WebCrawler.getLinksToCrawl().size(), 61);
        Assert.assertEquals(WebCrawler.getVisitedLinks().size(), 62);
        verify(executor, times(61)).execute(isA(Runnable.class));
        verify(webPageProcessor).enqueue(testPage);
    }

    private PageData getPage() throws Exception {
        PageData testPage = spy(new PageData());
        InputStream is = getClass().getClassLoader().getResourceAsStream("test.html");
        String myString = IOUtils.toString(is, "UTF-8");
        Document doc = Jsoup.parse(myString, testLink);
        doReturn(doc).when(testPage).getDocument(testLink);
        testPage.getPageData(testLink);
        return testPage;
    }
}
