package webc;


import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by ozzie on 10/2/15.
 */

/**
 * Sample PageProcessor that is used to find a word on a page ;
 * See WebPageProcessor for usage .
 */

public class FindWordPageProcessor implements PageProcessor {
    private final Logger logger = LoggerFactory.getLogger(FindWordPageProcessor.class);
    private String wordToFind;
    private boolean found = false;

    public FindWordPageProcessor(String lostWord) {
        wordToFind = lostWord;
    }

    public String getWordToFind() {
        return wordToFind;
    }

    public void setWordToFind(String wordToFind) {
        this.wordToFind = wordToFind;
    }

    public void processPage(PageData page) {
        logger.debug("Processing");
        if (page.getHtmlDocument() != null && !found) {
            if (StringUtils.containsIgnoreCase(page.getHtmlDocument().text(), wordToFind)) {
                logger.info("WordFound {} found on page at URL {}", wordToFind, page.getPageUrl());
                found = true;
            }
        } else {
            logger.debug("Error Loading body of page {}", page.getPageUrl());
        }
    }

    public boolean processingCompleted() {
        return found;
    }
}
