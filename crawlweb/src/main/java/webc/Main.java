package webc;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Created by ozzie on 10/2/15.
 */
public class Main {

    public static void main(String[] args) {
       /* Thread pool to be used for web page processing */
        ExecutorService webexecutor = Executors.newFixedThreadPool(4);
        /* Thread pool to be used for web crawling*/
        ExecutorService executor = Executors.newFixedThreadPool(8);
        /*Queue for holding web pages to be processed */
        BlockingQueue<PageData> queue = new LinkedBlockingQueue<PageData>();
        /*WebPage Processor Setup */
        WebPageProcessor.setQueue(queue);
        /*Registering  page processors */
        WebPageProcessor.registerPageProcessor(new FindWordPageProcessor("mystery"));
        WebPageProcessor.registerPageProcessor(new FindWordPageProcessor("java"));
        WebPageProcessor.setExecutor(webexecutor);

        /*Initializing web crawler*/
        WebCrawler.init("http://www.ucf.edu");
        WebCrawler.setExecutor(executor);
       /*Lets go crawling and processing*/
        WebCrawler crawler = new WebCrawler();
        crawler.setWebPageProcessor(new WebPageProcessor());
        executor.execute(crawler);
        while (!webexecutor.isTerminated()) {
            if (WebPageProcessor.processingComplete()) {
                webexecutor.shutdown();
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        executor.shutdownNow();
        System.out.println("Processing Complete");
    }
}
