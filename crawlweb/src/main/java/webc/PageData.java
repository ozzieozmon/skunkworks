package webc;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by ozzie on 10/2/15.
 */

/**
 * Contains Page Data contains page data for processed paged
 * on instantiation the url is connected to and the page retrived the url ,
 * paged body and the pagelinks are stored
 */
public class PageData {

    /*fake out webservers */
    private static final String USER_AGENT =
            "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.1 (KHTML, like Gecko) Chrome/13.0.782.112 Safari/535.1";
    final Logger logger = LoggerFactory.getLogger(PageData.class);
    /*list of links on the page */
    private List<String> links = new LinkedList<String>();
    /*page url*/
    private String pageUrl;
    /*If the page was retrieved*/
    private boolean found = false;
    /*Html Document from the page - contains the header , page body etc..*/
    private Document htmlDocument;


    protected Document getDocument(String url) throws IllegalArgumentException, IOException {
        Connection connection = Jsoup.connect(url).userAgent(USER_AGENT);
        return connection.get();
    }

    /**
     * Constructor
     *
     * @param url url to retrieve page data .
     */

    public void getPageData(String url) {
        try {
            setPageUrl(url);
            htmlDocument = getDocument(url);
            found = true;
            logger.debug("Received web page at {}", url);
            Elements linksOnPage = htmlDocument.select("a[href]");
            logger.debug("Found links {}", linksOnPage.size());
            for (Element link : linksOnPage) {
                links.add(link.absUrl("href"));
            }
        } catch (IllegalArgumentException e) {
            // Failed HTTP request
            logger.debug("Error in URL ", e);
        } catch (IOException e) {
            // Failed HTTP request
            logger.debug("Error in  HTTP request ", e);
        }
    }

    public String getPageUrl() {
        return pageUrl;
    }

    public void setPageUrl(String pageUrl) {
        this.pageUrl = pageUrl;
    }

    public List<String> getLinks() {
        return links;
    }

    /* hide constructor */

    public boolean isFound() {
        return found;
    }

    public Document getHtmlDocument() {
        return htmlDocument;
    }

}
