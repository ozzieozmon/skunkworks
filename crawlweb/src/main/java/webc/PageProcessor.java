package webc;

/**
 * Created by ozzie on 10/2/15.
 */

/**
 * Class Page Processor all classes hat need to provide web page processing must implement this interface.
 */


public interface PageProcessor {
    /**
     * Method that will be called for a page to be processed.
      *
     * @param page Page information to be processed
     */
    void processPage(PageData page);

    /**
     * Method to indicate if the processor has finished processing
     * */
    boolean processingCompleted();
}
