package webc;

/**
 * Created by ozzie on 10/2/15.
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;


/**
 * Class WebCrawler implements the logic for webcrawling
 * Crawls to the end of the web.
 */
public class WebCrawler implements Runnable {

    /*Queue shared across instances containing   links to be crawled*/
    private static ConcurrentLinkedQueue<String> linksToCrawl = new ConcurrentLinkedQueue<String>();

    /*Set shared across instances for crawled links*/
    private static Set<String> visitedLinks = new HashSet<String>();

    private static ExecutorService executor;

    final Logger logger = LoggerFactory.getLogger(WebCrawler.class);
    private WebPageProcessor webPageProcessor;

    public static void setVisitedLinks(Set<String> visitedLinks) {
        WebCrawler.visitedLinks = visitedLinks;
    }

    public static ConcurrentLinkedQueue<String> getLinksToCrawl() {
        return WebCrawler.linksToCrawl;
    }

    public static void setLinksToCrawl(ConcurrentLinkedQueue<String> linksToCrawl) {
        WebCrawler.linksToCrawl = linksToCrawl;
    }

    public static Set<String> getVisitedLinks() {
        return WebCrawler.visitedLinks;
    }

    public void setWebPageProcessor(WebPageProcessor webPageProcessor) {
        this.webPageProcessor = webPageProcessor;
    }

    public static void setExecutor(ExecutorService executor) {
        WebCrawler.executor = executor;
    }

    /**
     * Initialize crawler
     *
     * @param url Starting url
     */

    public static void init(String url) {
        linksToCrawl.add(url);
        visitedLinks.add(url);
    }

    protected PageData getPageData(String linkToCrawl) {
        PageData curPageData = new PageData();
        curPageData.getPageData(linkToCrawl);
        return curPageData;
    }

    /**
     * Crawling function
     **/
    public void crawl() {
        PageData curPageData;
        // breadth first search crawl of web
        if (!linksToCrawl.isEmpty()) {
            String linkToCrawl = linksToCrawl.remove();
            logger.debug("Link to Crawl {}", linkToCrawl);
            curPageData = getPageData(linkToCrawl);
            // only needed in case website does not respond
            if (curPageData.isFound()) {
                // find and print all matches
                for (String link : curPageData.getLinks()) {
                    if (visitedLinks.contains(link)) {
                        logger.debug("URL previously visited {}", link);
                    } else {
                        linksToCrawl.add(link);
                        visitedLinks.add(link);
                        crawlMoreLinks();
                    }
                }
                /*Add page to Page processing Queue */
                webPageProcessor.enqueue(curPageData);
                logger.debug("links to be processed, {} links visited {} ", linksToCrawl.size(), visitedLinks.size());
            }
        }
    }

    private void crawlMoreLinks() {
        Runnable r = new Runnable() {
            public void run() {
                crawl();
            }
        };
        try {
            executor.execute(r);
        } catch (RejectedExecutionException e) {
            System.out.println("Processing complete shutting down");
        }
    }

    public void run() {
        crawl();
    }
}