package webc;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.RejectedExecutionException;

/**
 * Created by ozzie on 10/2/15.
 */

/**
 * Class to process web pages
 * PageProcessors must be registered
 */
public class WebPageProcessor {
    /*List of Page Processors*/
    private static List<PageProcessor> pageProcessors = new ArrayList<PageProcessor>();
    /*Work queue for page data to be processed*/
    private static BlockingQueue<PageData> pagequeue;
    /*Executor for the thread pool to be used for processing pages */
    private static ExecutorService executor;

    final Logger logger = LoggerFactory.getLogger(WebPageProcessor.class);

    public static void setExecutor(ExecutorService executor) {
        WebPageProcessor.executor = executor;
    }

    public static BlockingQueue getQueue() {
        return pagequeue;
    }

    public static void setQueue(BlockingQueue<PageData> queue) {
        pagequeue = queue;
    }


    /**
     * registerPageProcessor All page processors must call this function before
     * they will be passed pages for processing
     *
     * @param pageProcessor Page processor to be registered
     */
    public static void registerPageProcessor(PageProcessor pageProcessor) {
        pageProcessors.add(pageProcessor);
    }

    /***
     * Add  page to the processing queue and makes a request to the thread pool for
     * processing.
     *
     * @param pageData Page for Processing
     */
    public void enqueue(PageData pageData) {
        try {
            pagequeue.put(pageData);
            process();
        } catch (InterruptedException e) {
            logger.debug("Processing Interrupted System Shutting Down?", e);
        } catch (RejectedExecutionException e) {
            System.out.println("Processing complete shutting down");
        }
    }

    public static boolean processingComplete() {
        boolean finished = true;
        for (PageProcessor pageProcessor : pageProcessors) {
            finished = finished && pageProcessor.processingCompleted();
        }
        return finished;
    }

    private void processPage(PageData in) {
        for (PageProcessor pageProcessor : pageProcessors) {
            pageProcessor.processPage(in);
        }
    }

    private void process() {
        Runnable r = new Runnable() {
            public void run() {
                processPage();
            }
        };
        try {
            executor.execute(r);
        } catch (RejectedExecutionException e) {
            System.out.println("Processing complete shutting down");
        }
    }

    public void processPage() {
        try {
            if (!pagequeue.isEmpty()) {
                PageData pageData = pagequeue.take();
                if (pageData != null) {
                    processPage(pageData);
                }
            }
        } catch (InterruptedException e) {
            logger.debug("Processing Interrupted ", e);
        }

    }
}
